# SoundCrawler

## Setting up

- install dependencies: sidekiq, redis, pgsql
- bundle install
- bundle exec rake db:setup
- yarn install 

## Running it
- webserver: `rails s`
- background processing: `sidekiq -q scrape_development_default -c 10`
- asset compilation: `./bin/webpack-dev-server`
- tests: `bundle exec guard`