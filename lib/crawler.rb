require 'soundcloud'
require 'pry'
require 'activerecord-import'

class Crawler 

	def initialize start_id=10602950, to_depth=1
		@options = {
			client_id: '913a9167d698d2d70c67d2d5a8561d65',
			client_secret: '9c162a15f44701d5a3410991188cf8f4'
		}

		@client = Soundcloud.new(client_id: @options[:client_id])
		@cursor = nil
		@followed_users = []
		@start_id = start_id
		@depth = 0
		@to_depth = to_depth
	end

	def crawl

		return if @depth === @to_depth

		initial = @client.get("/users/#{@start_id}")

		initial_user = User.find_or_initialize_by(user_id: initial.id)

		initial_user.data = initial

		initial_user.save

		loop do

			puts @cursor

			fres = get_page(initial.id, @cursor)

			@followed_users.push(fres.collection)
			
			break unless fres.next_href;

			@cursor = get_query(fres.next_href)['cursor']

		end

		@followed_users.flatten!

		users = @followed_users.map do |f|
			followed = User.new(user_id: f.id)
			followed.user_id = f.id
			followed.data = f
			followed
		end



		user_imports = User.import(users, on_duplicate_key_update: {conflict_target: [:user_id], columns: :all})

		followings = user_imports.ids.map do |f|
			Following.new(follower: initial_user, followed_id: f)
		end

		Following.import(followings, on_duplicate_key_ignore: {conflict_target: [:followed_id, :follower_id]})


		# @followed_users.each do |f|
		# 	Crawler.new(f.id, @depth + 1).crawl
		# end
	end

	private
		def get_page id, cursor
			@client.get("/users/#{id}/followings", cursor: @cursor)
		end

		def get_query uri
			uri = URI.parse(uri) 
			query = uri.query.split('&').map {|q| q.split('=') }.to_h
		end
end