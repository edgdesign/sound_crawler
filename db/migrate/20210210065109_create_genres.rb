class CreateGenres < ActiveRecord::Migration[5.2]
  def change
    create_view :genres, materialized: true
    add_index :genres, :genre, unique: true
    add_index :genres, :track_count
  end
end
