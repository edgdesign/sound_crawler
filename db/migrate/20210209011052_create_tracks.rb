class CreateTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :tracks do |t|
      t.references :user
      t.references :track, index: {unique: true}
      t.integer :favoritings_count, :comment_count, :reposts_count, :download_count, :playback_count
      t.json :data, default: {}

      t.timestamps
    end
  end
end
