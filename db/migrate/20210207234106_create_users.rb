class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
    	t.integer :user_id, index: {unique: true}
    	t.json :data, default: {}
      t.timestamps
    end
  end
end
