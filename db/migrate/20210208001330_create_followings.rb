class CreateFollowings < ActiveRecord::Migration[5.2]
  def change
    create_table :followings do |t|
    	t.references :followed
    	t.references :follower
    	
      t.timestamps
    end
    add_index :followings, [:followed_id, :follower_id], unique: true
  end
end
