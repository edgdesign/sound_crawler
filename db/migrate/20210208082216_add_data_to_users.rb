class AddDataToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :permalink, :string
  	add_column :users, :track_count, :integer
  	add_column :users, :followers_count, :integer
  	add_column :users, :playlist_count, :integer
  end
end
