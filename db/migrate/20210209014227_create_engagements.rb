class CreateEngagements < ActiveRecord::Migration[5.2]
  def change
    create_view :engagements, materialized: true
    add_index :engagements, :user_id
    add_index :engagements, :avg_plays
    add_index :engagements, :avg_likes
    add_index :engagements, :avg_comments
    add_index :engagements, :avg_reposts
    add_index :engagements, :avg_downloads

  end
end
