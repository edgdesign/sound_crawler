class CreateUserGenres < ActiveRecord::Migration[5.2]
  def change
    create_view :user_genres, materialized: true
		add_index :user_genres, :user_genre
		add_index :user_genres, :user_id
		add_index :user_genres, :track_count
    add_index :user_genres, [:user_genre, :user_id], unique: true
  end
end
