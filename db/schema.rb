# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_14_045311) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "followings", force: :cascade do |t|
    t.bigint "followed_id"
    t.bigint "follower_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id", "follower_id"], name: "index_followings_on_followed_id_and_follower_id", unique: true
    t.index ["followed_id"], name: "index_followings_on_followed_id"
    t.index ["follower_id"], name: "index_followings_on_follower_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "track_id"
    t.integer "favoritings_count"
    t.integer "comment_count"
    t.integer "reposts_count"
    t.integer "download_count"
    t.integer "playback_count"
    t.json "data", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["track_id"], name: "index_tracks_on_track_id", unique: true
    t.index ["user_id"], name: "index_tracks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer "user_id"
    t.json "data", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "permalink"
    t.integer "track_count"
    t.integer "followers_count"
    t.integer "playlist_count"
    t.index ["user_id"], name: "index_users_on_user_id", unique: true
  end


  create_view "engagements", materialized: true, sql_definition: <<-SQL
      SELECT users.id AS user_id,
      count(tracks.id) AS total_tracks,
      trunc(div((sum(tracks.playback_count))::numeric, (count(tracks.id))::numeric), 0) AS avg_plays,
      trunc(div((sum(tracks.favoritings_count))::numeric, (count(tracks.id))::numeric), 0) AS avg_likes,
      trunc(div((sum(tracks.comment_count))::numeric, (count(tracks.id))::numeric), 0) AS avg_comments,
      trunc(div((sum(tracks.reposts_count))::numeric, (count(tracks.id))::numeric), 0) AS avg_reposts,
      trunc(div((sum(tracks.download_count))::numeric, (count(tracks.id))::numeric), 0) AS avg_downloads
     FROM (tracks
       JOIN users ON ((users.id = tracks.user_id)))
    GROUP BY users.id;
  SQL
  add_index "engagements", ["avg_comments"], name: "index_engagements_on_avg_comments"
  add_index "engagements", ["avg_downloads"], name: "index_engagements_on_avg_downloads"
  add_index "engagements", ["avg_likes"], name: "index_engagements_on_avg_likes"
  add_index "engagements", ["avg_plays"], name: "index_engagements_on_avg_plays"
  add_index "engagements", ["avg_reposts"], name: "index_engagements_on_avg_reposts"
  add_index "engagements", ["user_id"], name: "index_engagements_on_user_id"

  create_view "similarities", sql_definition: <<-SQL
      SELECT target.id AS target_id,
      subject.id AS subject_id,
      count(f2.id) AS target_follows,
      count(f2.id) AS subject_follows
     FROM (((users target
       JOIN followings f ON ((target.id = f.follower_id)))
       JOIN followings f2 ON ((f.followed_id = f2.followed_id)))
       JOIN users subject ON ((f2.follower_id = subject.id)))
    WHERE (subject.id <> target.id)
    GROUP BY target.id, subject.id;
  SQL
  create_view "genres", materialized: true, sql_definition: <<-SQL
      SELECT (t.data ->> 'genre'::text) AS genre,
      count((t.data ->> 'genre'::text)) AS track_count
     FROM tracks t
    WHERE ((t.data ->> 'genre'::text) <> ''::text)
    GROUP BY (t.data ->> 'genre'::text);
  SQL
  add_index "genres", ["genre"], name: "index_genres_on_genre", unique: true
  add_index "genres", ["track_count"], name: "index_genres_on_track_count"

  create_view "tags", sql_definition: <<-SQL
      SELECT u.id AS user_id,
      (t.data ->> 'tag_list'::text) AS tags
     FROM (tracks t
       JOIN users u ON ((u.id = t.user_id)))
    GROUP BY (t.data ->> 'tag_list'::text), u.id;
  SQL
  create_view "user_genres", materialized: true, sql_definition: <<-SQL
      SELECT u.id AS user_id,
      (t.data ->> 'genre'::text) AS user_genre,
      count((t.data ->> 'genre'::text)) AS track_count
     FROM (tracks t
       JOIN users u ON ((u.id = t.user_id)))
    WHERE ((t.data ->> 'genre'::text) <> ''::text)
    GROUP BY (t.data ->> 'genre'::text), u.id;
  SQL
  add_index "user_genres", ["track_count"], name: "index_user_genres_on_track_count"
  add_index "user_genres", ["user_genre", "user_id"], name: "index_user_genres_on_user_genre_and_user_id", unique: true
  add_index "user_genres", ["user_genre"], name: "index_user_genres_on_user_genre"
  add_index "user_genres", ["user_id"], name: "index_user_genres_on_user_id"

end
