SELECT
	u.id as user_id,
	t.data ->> 'tag_list' as tags
FROM tracks as t
JOIN users as u on u.id = t.user_id
group by t.data ->> 'tag_list', u.id