SELECT
	t.data ->> 'genre' as genre,
	COUNT(t.data ->> 'genre') as track_count
FROM tracks as t
where
	t.data->> 'genre' != ''
group by t.data ->> 'genre'