SELECT
	users.id as user_id,
	COUNT(tracks.id) as total_tracks,
	TRUNC(DIV(SUM(tracks.playback_count),COUNT(tracks.id)), 0)  as avg_plays,
	TRUNC(DIV(SUM(tracks.favoritings_count),COUNT(tracks.id)), 0) as avg_likes,
	TRUNC(DIV(SUM(tracks.comment_count),COUNT(tracks.id)), 0) as avg_comments,
	TRUNC(DIV(SUM(tracks.reposts_count),COUNT(tracks.id)), 0) as avg_reposts,
	TRUNC(DIV(SUM(tracks.download_count),COUNT(tracks.id)), 0) as avg_downloads

FROM tracks
JOIN users ON users.id = tracks.user_id
GROUP BY users.id