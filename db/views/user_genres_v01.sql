SELECT
	u.id as user_id,
	t.data ->> 'genre' as user_genre,
	COUNT(t.data ->> 'genre') as track_count
FROM tracks as t
JOIN users as u on u.id = t.user_id
where
	t.data->> 'genre' != ''
group by t.data ->> 'genre', u.id