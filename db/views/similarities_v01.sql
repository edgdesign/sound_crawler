SELECT
	target.id as target_id,
	subject.id as subject_id,
	COUNT(f2.id) as target_follows,
	COUNT(f2.id) as subject_follows
FROM users as target
JOIN followings as f on target.id = f.follower_id
join followings as f2 on f.followed_id = f2.followed_id
JOIN users as subject ON f2.follower_id = subject.id
where subject.id != target.id
group by target.id, subject.id