Rails.application.routes.draw do

	mount ActionCable.server => '/cable'

  get 'graph', to: 'graph#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'application#index'


  resources :genres, only: [:index, :show]
	resources :users do 
		collection do 
			get :search
		end
	end
end
