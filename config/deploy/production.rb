set :stage, :production

server = %w{ubuntu@54.149.227.141}
role :app, server
role :web, server
role :db,  server
set :rails_env, 'production'

set :sidekiq_options, "-q scrape_production -q scrape_production_default"

 set :ssh_options, {
   keys: %w(~/.ssh/matts_key.pem),
   forward_agent: true,
   auth_methods: %w(publickey)
 }
