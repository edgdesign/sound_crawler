require 'test_helper'

class ScraperUserJobTest < ActiveJob::TestCase
	test 'scrapes users from soundcloud' do

		@user = User.create permalink: 'foo'

		mock = Minitest::Mock.new
		cable_mock = Minitest::Mock.new

		cable_mock.expect :call, true do |channel, message|
			channel == 'scrape:foo' &&
			message.is_a?(String)
		end
		
		def mock.user id; 
			Hashie::Mash.new({
				id: '2', 
				permalink: 'foo'
			})
		end

		def mock.tracks id
			[{
				id: '4',
				title: 'bar'
			}, {
				id: '5',
				title: 'bat'
			}, {
				id: '6',
				title: 'bab'
			}].map {|f| Hashie::Mash.new(f)}
		end
		
		def mock.followings id
			[{
				id: '3',
				permalink: 'baz'
			}].map {|t| Hashie::Mash.new(t)}
		end

		ActionCable.server.stub :broadcast, cable_mock do
			SoundcloudClient.stub :new, mock do		
				assert_difference 'User.count', 1 do
					ScrapeUserJob.perform_now(@user)
				end
			end
		end

		assert_mock cable_mock
	end
end