require 'test_helper'

class TrackTest < ActiveSupport::TestCase

	setup do 
		@track = tracks :one
	end

	test 'update data on create' do
		track = Track.new user_id: users(:matt).id

		mock = Minitest::Mock.new
		mock.expect(:call, true)

		track.stub :update_data, mock do 
			track.save
		end

		assert_mock mock
	end

	test 'update data on update' do
		mock = Minitest::Mock.new
		mock.expect(:call, true)

		@track.stub :update_data, mock do 
			@track.save
		end

		assert_mock mock
	end

	test '#update_data' do 
		data = {
			foo: 'bar',
			baz: 'bat',
			id: 100,
			user_id: 2,
		  track_id: 100,
		  favoritings_count: 100,
		  comment_count: 100,
		  reposts_count: 100,
		  download_count: 100,
		  playback_count: 100
		}

		@track.data = data
		@track.save

		assert_equal 100, @track.track_id
		assert_not_equal 100, @track.id
		assert_not_equal 2, @track.user_id

		assert_equal 100, @track.favoritings_count
		assert_equal 100, @track.comment_count
		assert_equal 100, @track.reposts_count
		assert_equal 100, @track.download_count
		assert_equal 100, @track.playback_count
	end
end
