require 'test_helper'

class UserTest < ActiveSupport::TestCase
	include ActiveJob::TestHelper
	setup do 
		@user = users :matt
	end

	test '#scrape' do 
		assert_enqueued_with job: ScrapeUserJob do 
			@user.scrape
		end
	end

	test 'update data on create and' do
		user = User.new

		mock = Minitest::Mock.new
		mock.expect(:call, true)

		user.stub :update_data, mock do 
			user.save
		end

		assert_mock mock
	end

	test 'update data on update' do 
		@user.save

		mock = Minitest::Mock.new
		mock.expect(:call, true)

		@user.stub :update_data, mock do 
			@user.save
		end

		assert_mock mock
	end

	test '#update_data' do 
		data = {
			foo: 'bar',
			baz: 'bat',
			id: '100',
			permalink: 'abc'
		}

		@user.data = data
		@user.save

		assert_equal 'abc', @user.permalink
		assert_not_equal 100, @user.id 
	end

	test 'concrete associations' do 
		# behavior of associations with table
		# backed modls


		# track
		# following

		flunk
	end
end
