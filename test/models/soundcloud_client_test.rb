require 'test_helper'

class SoundcloudClientTest < ActiveSupport::TestCase

	Res = Struct.new(:collection, :next_href)

	%i{resolve user tracks followings}.each do |method|
		test "##{method} raises an exception with no value passed" do
			assert_raises ArgumentError do 
				SoundcloudClient.new.send(method)
			end
		end
	end

	%i{resolve user}.each do |method|
		test "##{method} calls the client" do
			mock = Minitest::Mock.new

			mock.expect :get, {foo: 'bar'} do |url, options|
				url == '/resolve' || url == '/users/foo'
			end

			Soundcloud.stub :new, mock do 
				res = SoundcloudClient.new.send(method, 'foo')
				assert res
				assert_equal res, {foo: 'bar'}
			end

			assert_mock mock
		end
	end

	%i{tracks followings}.each do |method|
		test "##{method} gets a paginated collection" do
			mock = Minitest::Mock.new
			mock.expect :get, Res.new([{foo: 'bar'}], 'https://foo.com?cursor=bar') do |url, options|
				url == '/users/foo/tracks' ||
				url == '/users/foo/followings'
			end

			mock.expect :get, Res.new([{foo: 'bar'}]) do |url, options|
				options[:cursor] == 'bar'
			end

			Soundcloud.stub :new, mock do 
				res = SoundcloudClient.new.send(method, 'foo')
				assert_equal [{foo: 'bar'}, {foo: 'bar'}], res
			end

			assert_mock mock
		end
	end
end
