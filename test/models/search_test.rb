require 'test_helper'

class SearchTest < ActiveSupport::TestCase
  
  test 'Tuple type' do 
  	type = Search::Tuple.new

  	assert_equal 1...Float::INFINITY, type.cast([1])
  	assert_equal 1...10, type.cast([1, 10])
  	assert_equal 0...10, type.cast([nil, 10])
  	assert_nil type.cast(nil)
  	assert_nil type.cast('')
  end

  test 'initialize with default values' do 
  	search_attrs = Search.new.attributes

  	assert_equal '', search_attrs["permalink"]
  	assert_equal 0...10000, search_attrs["avg_plays"]
  	assert_equal 0...10000, search_attrs["avg_likes"]
  	assert_equal 0...Float::INFINITY, search_attrs["avg_comments"]
  	assert_equal 0...Float::INFINITY, search_attrs["avg_downloads"]
  	assert_equal 0...Float::INFINITY, search_attrs["avg_reposts"]
  	assert_equal 1, search_attrs["page"]

		search_attrs = Search.new({
			permalink: 'foo',
			avg_plays: [1, 10]
		}).attributes


  	assert_equal 'foo', search_attrs["permalink"]
  	assert_equal 1...10, search_attrs["avg_plays"]
  end

  test 'returns scoped results' do 
  	results = Search.new.results
  	assert_kind_of ActiveRecord::Relation, results
  end
end
