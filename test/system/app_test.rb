require "application_system_test_case"

class AppTest < ApplicationSystemTestCase

  setup do 
  	[Engagement, Genre, UserGenre].map &:refresh
  end

  test 'search for a user' do 
  	visit root_path
  	assert_selector "div.container.catalog"

		within 'div#search' do 
			fill_in 'search', with: 'mhm'
		end
		take_screenshot

		within '#search' do 
			click_link 'avatar'
		end

		assert_selector "div.container.profile"
  end

  test 'visiting the profile' do 
  	visit '/#/freeedy'
  	assert_selector "div.container.profile"
  	take_screenshot
  end

  test 'visiting the profile genres' do 
  	visit '/#/freeedy'
  	assert_selector "div.container.profile"
		within '.tab-links' do 
			click_link 'Genres'
		end
  	take_screenshot
  end

  test 'visiting the profile tracks' do 
  	visit '/#/freeedy'
  	assert_selector "div.container.profile"
		within '.tab-links' do 
			click_link 'Tracks'
		end
  	take_screenshot
  end

  test 'visiting genres' do
  	visit root_path
		within 'nav' do 
			click_link 'Genres'
		end
		assert_selector 'div.container.genres'

		take_screenshot
  end

  test 'visiting a genre' do
  	visit '/#/genre'
  	assert_selector 'div.container.genres'
  	click_link 'jazz'
  	assert_selector 'div.container.genre'

  	take_screenshot
  end

	test "visiting the catalog" do
		visit root_path
		assert_selector "div.container.catalog"
		take_screenshot
	end
end
