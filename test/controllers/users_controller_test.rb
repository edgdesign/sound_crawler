require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "index" do
    get users_url
    assert_response :success
  end

  test "search" do
    get search_users_url
    assert_response :success
  end

  test "showing a user from db" do

  	matt = users(:matt)

  	get user_url(matt.permalink)

  	assert_response :success
  end

  test 'showing a user we have to query SC for' do 
  	mock = Minitest::Mock.new

  	ret = Hashie::Mash.new({
  		id: 1,
  		parmalink: 'foo'
  	})

  	mock.expect :resolve, ret, ['foo']

  	SoundcloudClient.stub :new, mock do 
  		get user_url('foo')
  	end

		assert_response :success
		assert_mock mock
  end

  test 'showing a user that 404s on sc' do 
  	mock = Minitest::Mock.new

  	ret = Hashie::Mash.new({
  		id: 1,
  		parmalink: 'foo'
  	})

  	def mock.resolve id
  		raise Soundcloud::ResponseError.new('404')
  	end

  	SoundcloudClient.stub :new, mock do 
  		get user_url('foo')
  	end

		assert_response :not_found
  end

  test "create a user we already have in the db" do
  	ScrapeUserJob.stub :perform_later, true do 
  		get user_url('foo')
	  end
		assert_response :success
  end

  test "create a user need to find from sc" do
  	mock = Minitest::Mock.new

  	ret = Hashie::Mash.new({
  		id: 1,
  		parmalink: 'foo'
  	})

  	mock.expect :resolve, ret, ['foo']

  	ScrapeUserJob.stub :perform_later, true do 
	  	SoundcloudClient.stub :new, mock do 
	  		get user_url('foo')
	  	end
	  end

		assert_response :success
		assert_mock mock
  end

  test "create a user that 404s" do
  	mock = Minitest::Mock.new

  	ret = Hashie::Mash.new({
  		id: 1,
  		parmalink: 'foo'
  	})

  	def mock.resolve id
  		raise Soundcloud::ResponseError.new('404')
  	end

  	SoundcloudClient.stub :new, mock do 
  		post users_url, params: { permalink: 'foo' } 
  	end

		assert_response :not_found
  end
end
