class Engagement < View
	self.table_name = 'engagements';

	belongs_to :user, optional: true
end