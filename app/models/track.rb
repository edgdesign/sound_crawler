class Track < ApplicationRecord
  belongs_to :user

  before_save(:update_data, on: [:create, :update])

	def update_data
		self.attributes = data.slice(*self.attributes.keys).except('id', 'user_id')
	end
end
