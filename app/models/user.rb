class User < ApplicationRecord

 	# users following me
	has_many :follows,
		class_name: 'Following',
		foreign_key: 'followed_id',
		inverse_of: :followed
	
	# user i follow
	has_many :followings,
		class_name: 'Following',
		foreign_key: 'follower_id',
		inverse_of: :follower,
		dependent: :destroy

	has_many :followed_users, 
		through: :followings,
		source: :followed
	
	has_many :followers,
		through: :follows,
		source: :follower

	has_many :tracks, dependent: :destroy
	has_one :engagement
	
	has_many :user_genres
	has_many :genres, through: :user_genres, source: :genre

	has_many :tags

	has_many :similarities, -> { 
		order(target_follows: :desc)
	}, foreign_key: :subject_id
	has_many :similar_users, through: :similarities, source: :target

	before_save(:update_data, on: [:create, :update])

	def scrape
		ScrapeUserJob.perform_later(self)
	end

	def update_data
		self.attributes = data.slice(*self.attributes.keys).except('id')
		self.user_id = data.slice('id')['id']
	end
end
