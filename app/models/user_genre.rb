class UserGenre < View
	self.table_name = 'user_genres';
	self.primary_key = 'user_genre'

	belongs_to :user
	belongs_to :genre, foreign_key: :user_genre

	def self.default_scope
		order(track_count: :desc)
	end

	def read_only?
		true
	end
end