class Following < ApplicationRecord
	belongs_to :follower, class_name: 'User', inverse_of: :followings
	belongs_to :followed, class_name: 'User', inverse_of: :follows
end
