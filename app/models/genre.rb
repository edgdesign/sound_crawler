class Genre < View
	self.table_name = 'genres';
	self.primary_key = 'genre'

	has_many :user_genres, foreign_key: :user_genre
	has_many :users, through: :user_genres, source: :user

	def self.default_scope
		order(track_count: :desc)
	end
end