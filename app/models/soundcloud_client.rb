class SoundcloudClient
	attr_accessor :client
	def initialize
		@options = Rails.application.credentials.soundcloud!

		@client = Soundcloud.new(client_id: @options[:client_id])
	end

	def resolve query
		raise ArgumentError.new 'Missing query' if !query 
		client.get '/resolve', url: "http://soundcloud.com/#{query}"
	end

	def user user_id
		raise ArgumentError.new 'Missing user id' if !user_id 
		client.get "/users/#{user_id}"
	end

	def tracks user_id
		raise ArgumentError.new 'Missing user id' if !user_id 
		get_paginated_collection("/users/#{user_id}/tracks", linked_partitioning: 1, limit: 200)
	end

	def followings user_id
		raise ArgumentError.new 'Missing user id' if !user_id 
		get_paginated_collection("/users/#{user_id}/followings")
	end


	private

	def get_paginated_collection url, options={}
		collection = []
		cursor = nil
		loop do 

			res = client.get(url, options.merge(cursor: cursor))

			collection.push res.collection

			break unless res.next_href

			cursor = get_cursor(res.next_href)

		end

		return collection.flatten
	end

	def get_query uri
		uri = URI.parse(uri) 
		query = uri.query.split('&').map {|q| q.split('=') }.to_h
	end

	def get_cursor url
		URI.decode(get_query(url)['cursor'])
	end
end