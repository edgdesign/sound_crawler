class Tag < View
	self.table_name = 'tags';

	belongs_to :user, optional: true
end