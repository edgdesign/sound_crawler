class Search
	include ActiveModel::Model
	include ActiveModel::Attributes

	class Tuple < ActiveModel::Type::String
		def cast value
			return nil unless value.present?
			first = value[0].present? ? value[0].to_i : 0
			last = value[1].present? ? value[1].to_i : Float::INFINITY
			return first...last
		end
	end

	attribute :permalink, :string
	attribute :avg_plays, Tuple.new
	attribute :avg_likes, Tuple.new
	attribute :avg_comments, Tuple.new
	attribute :avg_downloads, Tuple.new
	attribute :avg_reposts, Tuple.new
	attribute :page, :integer

	DEFAULTS = {
		permalink: '',
		avg_plays: [0, 10000],
		avg_likes: [0, 10000],
		avg_comments: [0, nil],
		avg_downloads: [0, nil],
		avg_reposts: [0, nil],
		page: 1
	}

	def self.search params = DEFAULTS
		self.new(params)
	end

	def initialize params = DEFAULTS
		super DEFAULTS.merge(params.slice(*DEFAULTS.keys)).with_indifferent_access
	end

	def results
		scope = User.joins(:engagement)
		scope = scope.where(permalink: permalink) unless !permalink || permalink.blank?
		scope = scope.where(engagements: conditions)
		scope = scope.order(avg_plays: :desc)
		scope = scope.page(page).per(100)						
		scope
	end

	private

	def conditions
		self.attributes.except('permalink', 'page').reduce({}) do |memo, a|
			memo[a[0]] = a[1] unless a[1].nil?
			memo
		end
	end
end