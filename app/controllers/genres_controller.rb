class GenresController < ApplicationController

	def index
		@genres = genres_source.page(genre_params[:page]).per(100)
	end

	def show
		@genre = genres_source.includes(:user_genres).find_by_genre(genre_params[:id])
	end

	private

	def genre_params
		params.permit!
	end

	def genres_source
		Genre
	end 
end
