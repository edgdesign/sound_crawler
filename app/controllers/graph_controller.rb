class GraphController < ApplicationController
  def index
  	@nodes = User.where(id: params[:ids]).flat_map do |user|
  		[user, user.similar_users].flatten
  	end.uniq

  	@edges = Following.where(followed_id: @nodes, follower_id: @nodes)

  	render json: {nodes: @nodes, edges: @edges}

  end
end
