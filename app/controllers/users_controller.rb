class UsersController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound, Soundcloud::ResponseError do |e|
		head 404
	end

	def index
		@search = Search.new(user_params)
	end

	def search
		@users = User.where("permalink like ?", "%#{user_params[:permalink]}%").limit(10)
	end

	def show
		@user = users_source.find_by_permalink user_params[:id]
		@user ||= User.create(data: sc_client.resolve(user_params[:id]))
		@similar_users = @user.similarities.includes(:target).take(50)
	end

	def create

		@user = User.find_by(permalink: user_params[:permalink])

		unless @user
			@user = User.create(data: sc_client.resolve(user_params[:permalink]))
		end

		ScrapeUserJob.perform_later(@user)

		render :show
	end

	private

	def user_params
		params.permit!
	end

	def sc_client
		client ||= SoundcloudClient.new
	end

	def users_source
		User.includes(:tracks, :engagement)
	end
end
