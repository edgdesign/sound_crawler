json.results @search.results do |result|
	json._id result.id
	json.permalink result.permalink
	json.avatar_url result.data['avatar_url']
	json.data result.data
	json.engagement do
		json.avg_plays result.engagement.avg_plays.to_f
		json.avg_likes result.engagement.avg_likes.to_f
		json.avg_comments result.engagement.avg_comments.to_f
		json.avg_downloads result.engagement.avg_downloads.to_f
		json.avg_reposts result.engagement.avg_reposts.to_f
	end
end
json.page do
	json.page @search.page
	json.total_pages @search.results.total_pages
	json.total_count @search.results.total_count
end