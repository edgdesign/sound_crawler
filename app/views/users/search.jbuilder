json.array! @users do |user|
	json._id user.id
	json.permalink user.permalink
	json.data user.data
end