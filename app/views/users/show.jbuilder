json._id @user.id
json.permalink @user.permalink
json.data @user.data
if @user.engagement
	json.engagement do
		json.avg_plays @user.engagement.avg_plays.to_f
		json.avg_likes @user.engagement.avg_likes.to_f
		json.avg_comments @user.engagement.avg_comments.to_f
		json.avg_downloads @user.engagement.avg_downloads.to_f
		json.avg_reposts @user.engagement.avg_reposts.to_f
	end
end
json.genres @user.user_genres do |g|
	json.genre g.user_genre
	json.track_count g.track_count
end

json.similar_users @similar_users do |s|
	json.follows s.target_follows
	json._id s.target.id
	json.permalink s.target.permalink
	json.data s.target.data
end

json.tracks @user.tracks do |t|
	json.data t.data
end
