json.genre @genre.genre
json.track_count @genre.track_count

json.users @genre.user_genres.limit(100) do|user_genre|
	json._id user_genre.user.id
	json.track_count user_genre.track_count
	json.permalink user_genre.user.permalink
	json.data user_genre.user.data
end