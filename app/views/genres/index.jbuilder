json.results @genres do |result|
	json._id result.id
	json.genre result.genre
	json.track_count result.track_count
end

json.page do
	json.page params[:page].to_i
	json.total_pages @genres.total_pages
	json.total_count @genres.total_count
end