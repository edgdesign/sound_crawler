class ScrapeChannel < ApplicationCable::Channel
	def subscribed
		stream_for "#{params[:permalink]}"
	end

	def ping
		ActionCable.server.broadcast('scrape:scrape', 'pong')
	end
end