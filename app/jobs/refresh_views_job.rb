class RefreshViewsJob < ApplicationJob
	def perform
		ActiveRecord::Base.connection_pool.with_connection do 
			Engagement.refresh
			UserGenre.refresh
			Genre.refresh
		end
	end
end