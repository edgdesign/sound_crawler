class ScrapeUserJob < ApplicationJob

	after_perform :refresh_materialized_views

	def perform(model, refresh=true)
		Rails.logger.info('Hello from scrape job')

		@refresh = refresh

		@user = client.user(model.user_id)

		@tracks = client.tracks(model.user_id)
		@followings = client.followings(model.user_id).uniq
		# in a couple cases we get errors because SC data is not clean and there are duplicate users
		# since the bulk update below requires a statement not to hit the same row twice we need to dedupe

		@model = model
		persist!

		ActionCable.server.broadcast("scrape:#{@model.permalink}", render_json)

		Rails.logger.info('Goodbye from scrape job')

	end

	def persist!
		persist_user
		persist_tracks
		persist_followings
	end

	private

	def refresh_materialized_views
		RefreshViewsJob.perform_later if @refresh
	end

	def persist_user
		@model.attributes = user_from_data(@user)
		@model.save
		@model.reload
	end

	def persist_tracks
		tracks = @tracks.map do |t| 
			@model.tracks.build(track_from_data(t)) 
		end

		Track.import(tracks, on_duplicate_key_update: {conflict_target: [:track_id], columns: :all})
	end

	def persist_followings
		followed_users = @followings.map {|f| User.new(user_from_data(f))}

		user_import = User.import(followed_users, on_duplicate_key_update: {conflict_target: [:user_id], columns: :all})

		followings = user_import.ids.map do |f|
			Following.new(follower: @model, followed_id: f)
		end

		Following.import(followings, on_duplicate_key_ignore: {conflict_target: [:followed_id, :follower_id]})
	end

	def client
		@client ||= SoundcloudClient.new
	end

	def track_from_data data
		return {
			track_id: data.id,
			data: sanitize_json(data),
			user_id: @user.id,
		}.merge(data.slice(*Track.attribute_names).except('id', 'user_id'))
	end

	def user_from_data data
		return {
			data: sanitize_json(data),
			user_id: data.id,
		}.merge(data.slice(*User.attribute_names).except('id'))
	end

	def render_json
		ApplicationController.render(
			template: 'users/show.jbuilder',
			assigns: {
				user: @model
			}
		)
	end

	def sanitize_json json
		JSON.parse(JSON.dump(json).gsub(/\\u0000/, ''))
	end
end
